﻿

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="style.css" media="screen"/>
<title>Расчет отопления</title>
</head>
<body>
<div class="container">
<?php
	$L = 10; // Длина рулона
	$D = 1; // Ширина рулона
	$W = 16; // периметр помещения
	$R = 3; // высота помещения
	echo "<h1>Добро пожаловать!</h1> <p> Здесь вы можете рассчитать количество рулонов.</p>"; 
	echo "<h3> Заданные параметры:</h3>"; 
	echo "<p> Длина рулона: $L м.</p>"; 
	echo "<p> Ширина рулона: $D м.</p>"; 
	echo "<p> Периметр помещения: $W вт.</p>";
	echo "<h3>  Результаты расчета:</h3>"; 
	echo "<p> Площадь комнаты: ". $W*$R ." кв.м.</p>"; 

	$a = (float)($W*$R)/($L*$D);
    $b = ceil($a);	// Вычисление количества секций исходя из нормы 100 ватт на один кв. метр
	echo "<p> Для данного помещения необходимо: $b рулонов.</p>";
?>

</div>
</body>
</html>