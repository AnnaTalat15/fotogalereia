 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="style.css" media="screen"/>
<title>Уведомление</title>
</head>
<body>
<?php
$theNames = array( '{name}','{text}','{data}');
$theValue = array(
$theValue1 = array ('Иван Иванович',
'О влиянии роста цен на интенсивность северного сияния ',
'12.10.2017'),
$theValue2 = array('Петр Петрович', 
'Автоморфизмы конгруэнтных многообразий',
'20.03.2018'));
$theTpl = '<h1> Уважаемый(ая), <i> {name} </i>!</h1>
<p> </p>
<p> Ваша статья " <i> {text} </i>" принята к публикации и выйдет в печать не позднее <i> {data} </i> года.</p>
<p> Дополнительную информацию Вы можете получить по телефону : +7(000)-000-00-00.</p>
<p> </p>
<p> С уважением, </p>
<p> Редакция журнала " Вести с аглебраических полей" </p>';
$resultText = str_replace( $theNames, $theValue1, $theTpl);
$resultText1 = str_replace( $theNames, $theValue2, $theTpl);
echo $resultText;
echo $resultText1;
?>

</body>
</html>